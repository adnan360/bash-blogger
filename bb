#!/bin/bash

cd `dirname $0`

# Config
if [ -f "inc/config.sh" ]; then
	source inc/config.sh
elif [ -f "inc/config.default.sh" ]; then
	source inc/config.default.sh
else
	echo "ERROR: Config not found!"
	exit 10
fi

# Store theme directory for later use
THEME_DIRECTORY="$INC_PATH/themes/$THEME_NAME"
# Get header and footer
HEADER_SOURCE=$(cat "$THEME_DIRECTORY/header.html") 
FOOTER_SOURCE=$(cat "$THEME_DIRECTORY/footer.html")
# Declare arrays for storing data about blog posts
declare -a DATA_TITLES
declare -a DATA_DESTFILES
declare -a DATA_DESCRIPTIONS

# Ref: https://gist.github.com/pkuczynski/8665367
# Adapted to accept input from stdin
_parse_yaml() {
   local prefix=$2
   local s='[[:space:]]*' w='[a-zA-Z0-9_]*' fs=$(echo @|tr @ '\034')
   echo "$1" | sed -ne "s|^\($s\)\($w\)$s:$s\"\(.*\)\"$s\$|\1$fs\2$fs\3|p" \
        -e "s|^\($s\)\($w\)$s:$s\(.*\)$s\$|\1$fs\2$fs\3|p"   |
   awk -F$fs '{
      indent = length($1)/2;
      vname[indent] = $2;
      for (i in vname) {if (i > indent) {delete vname[i]}}
      if (length($3) > 0) {
         vn=""; for (i=0; i<indent; i++) {vn=(vn)(vname[i])("_")}
         printf("%s%s%s=\"%s\"\n", "'$prefix'",vn, $2, $3);
      }
   }'
}

# Clean previous output
# Delete everything except the .keep file
find "$OUTPUT_PATH" -mindepth 1 -not -name '*.keep' -delete
# Copy assets directory
cp -r "$INPUT_PATH/assets" "$OUTPUT_PATH/"

# Go through blog posts
for FILE in $(ls "$INPUT_PATH"/*.md | sort -nr)
do
	DESTFILE=$(basename "$FILE" | cut -d. -f1)".html"
	echo "Processing $FILE..."

	BODY_SOURCE=$(cat "$FILE")
	# Ref: https://stackoverflow.com/a/28222257
	FRONT_MATTER=$(echo "$BODY_SOURCE" | sed -En '1 { /^---/ { :a N; /\n---/! ba; p} }')
	# Process YAML front-matter
	eval $(_parse_yaml "$FRONT_MATTER" "frontmatter_")
	# Strip the front-matter
	BODY_SOURCE=$(echo "$BODY_SOURCE" | sed '1 { /^---/ { :a N; /\n---/! ba; d} }')
	# Remove empty lines at the beginning and end of string
	# to not include line breaks (https://unix.stackexchange.com/a/552195)
	BODY_SOURCE=$(echo "$BODY_SOURCE" | sed -e '/./,$!d' -e :a -e '/^\n*$/{$d;N;ba' -e '}')
	# Parse markdown
	BODY_SOURCE=$(echo "$BODY_SOURCE" > sh inc/markdown.bash/markdown.sh)
	# Get title
	if [ -z "$frontmatter_title" ]; then
		TITLE=$(echo "$BODY_SOURCE" | head -n 1 | sed -n '/^$/!{s/<[^>]*>//g;p;}')
	else
		TITLE="$frontmatter_title"
	fi
	DESCRIPTION="$frontmatter_description"
	# Replace the values
	HEADER_SOURCE_PROCESSED=${HEADER_SOURCE//%%TITLE%%/$TITLE}
	HEADER_SOURCE_PROCESSED=${HEADER_SOURCE_PROCESSED//%%BLOG_TITLE%%/$BLOG_TITLE}
	echo "Outputting to $OUTPUT_PATH/$DESTFILE..."
	echo "$HEADER_SOURCE_PROCESSED $BODY_SOURCE $FOOTER_SOURCE" > "$OUTPUT_PATH/$DESTFILE"

	# For later
	DATA_TITLES+=("$TITLE")
	DATA_DESTFILES+=("$DESTFILE")
	DATA_DESCRIPTIONS+=("$DESCRIPTION")
done

# Prepare index.html
BODY_SOURCE=""
HEADER_SOURCE_PROCESSED=${HEADER_SOURCE//%%TITLE%%/$BLOG_TITLE}
HEADER_SOURCE_PROCESSED=${HEADER_SOURCE_PROCESSED//%%BLOG_TITLE%%/$BLOG_TITLE}

for ((i=0; i<${#DATA_TITLES[@]}; i++)); do
	BODY_SOURCE+="<div class=\"post-container\">"

	# Post title
	BODY_SOURCE+="<h2><a class=\"title\" href=\"${DATA_DESTFILES[$i]}\">${DATA_TITLES[$i]}</a></h2>"

	# Post description
	if [ ! -z "${DATA_DESCRIPTIONS[$i]}" ]; then
		BODY_SOURCE+="<p class=\"description\">${DATA_DESCRIPTIONS[$i]}</p>"
	fi

	BODY_SOURCE+="</div>"
done

echo "$HEADER_SOURCE_PROCESSED $BODY_SOURCE $FOOTER_SOURCE" > "$OUTPUT_PATH/index.html"

