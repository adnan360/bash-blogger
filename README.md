# Bash Blogger

No-dependency blogging solution written in Bash

- Depends only on bash + coreutils
- Supports markdown + yaml front matter
- Supports assets management
- Supports templating (customizable header, footer)
- Configurable

_NOTE: The project is in alpha stage. So expect breaking changes._


## Install

```
git clone https://gitlab.com/adnan360/bash-blogger.git
cd bash-blogger
git submodule init && git submodule update
```


## Configure

```
cp inc/config.default.sh inc/config.sh
vim inc/config.sh
```


## Usage

Put .md files on `input/` directory. Make sure you name them so that latest ones stay at the end:

```
1.md
2.md
3.md
```

or

```
20210114.md
20210116.md
20210119.md
```

Example post .md file can be: 

`input/20210124-some-title.md`:
```
---
title: Some title
author: Some author
description: Some short description of the blog post here.
---

# Some title

Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.

![Example Alt text](assets/20210124-some-title/example.png "Optional title")
```

Put images in `input/assets/` and inside preferably the same name as the .md file (but not required).

When ready, run below to generate blog on `public/`:

```
./bb
```
